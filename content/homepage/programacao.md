---
title: 'Programação'
weight: 2
header_menu: true
---

**28/10 -**
  [9:45h](#2810---945h) 
| [10:00h](#2810---1000h) 
| [14:00h](#2810---1400h) 
| [19:00h](#2810---1900h) 

**29/10 -**
  [10:00h](#2910---1000h) 
| [14:00h](#2910---1400h) 

-------------------------------

# 28/10 - 9:45h
##### Abertura do III Workshop de Computação Quântica - UFSC

> **Eduardo Duzzioni** (Coordenador) - 
Universidade Federal de Santa Catarina 

Para abrir o III Workshop de Computação Quântica - UFSC, convidamos você a conhecer um pouco da história do Workshop e do Grupo de Computação Quântica da UFSC. Nessa primeira conversa, vamos comentar sobre a importância da computação quântica no Brasil e no mundo, e como trabalhamos para produzir e divulgar ciência nesta área. Também reservamos um espaço para esclarecer dúvidas que você tenha sobre o evento.

<p align="center">
<iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/cbvitPmNveQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>

* Apresentação: [PDF](slides/eduardo.pdf)

-------------------------------

# 28/10 - 10:00h
#### Caracterização, simulação e boost de circuitos quânticos através de machine learning clássico

> [**Leandro Aolita**](#leandro-aolita) - Universidade Federal do Rio de Janeiro

Recentemente houve um progresso impressionante em computação quântica experimental, especialmente em circuitos quânticos. O resultado de décadas de pesquisa básica -- tanto teórica quanto experimental -- está finalmente a beira de virar uma tecnologia aplicável á resolução de problemas intratáveis com computadores clássicos, com um potencial transformador socioeconômico enorme. No entanto, a realização física de computadores quânticos de grande escala constitui ainda um grande desafio, por que as inevitáveis imperfeições experimentais rapidamente destroem as características quânticas dos circuitos, acabando com toda possível vantagem computacional sobre algoritmos clássicos. Nesse colóquio, falarei sobre avanços recentes tanto na caracterização e validação de circuitos quânticos, quanto na sua simulação clássica e fortalecimento frente a efeitos de ruído e limitações de conectividade. O denominador comum de todos esses avanços é que eles estão baseados ou motivados por modelos de redes neurais clássicas, nativos de aprendizado não-supervisionado. Esses resultados são relevantes para computação quântica no curto prazo.

<p align="center">
<iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/cbvitPmNveQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>

* Apresentação: [PDF](slides/leandro.pdf)

Para confirmar sua presença na palestra preencha a formulário https://forms.gle/tEYGtoQ39aLKqo1a8.

--------------------------------

# 28/10 - 14:00h
#### Computação quântica para iniciantes

> **Evandro Chagas** (Organização) -
Universidade Federal de Santa Catarina 

Se você tem curiosidade sobre computação quântica mas ainda não foi introduzido ao meio, convidamos você a participar da segunda apresentação do III Workshop de Computação Quântica - UFSC. Nela, vamos introduzir os principais conceitos e ferramentas da computação quântica de uma maneira simples, voltado para quem está começando na área.

<p align="center">
<iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/5bKulxli9YY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>

* Apresentação: [PDF](slides/evandro.pdf)

Para confirmar sua presença na palestra preencha a formulário https://forms.gle/Xy3zXUsjpcssfbDZ9.

--------------------------------

# 28/10 - 19:00h
#### Controle de sistemas quânticos com ruído: da pesquisa fundamental ao desenvolvimento de software para tecnologias quânticas 

> [**André Carvalho**](#andré-carvalho) - Q-Ctrl

Um dos grandes desafios práticos para a realização de computadores quânticos, e de outras tantas tecnologias baseadas em fenômenos quânticos, são os efeitos deletérios de ruído, instabilidades e erros no hardware. Nesse seminário discutiremos o efeito de ruído na performance de portas lógicas quânticas e soluções para o problema baseadas em controle quântico robusto. Definiremos o que consideramos robustez e como obter e implementar soluções robustas usando as ferramentas de software desenvolvidas na Q-CTRL. Finalizaremos mostrando experimentos realizados nos computadores quânticos da IBM utilizando dois tipos de estratégias para a criação de pulsos robustos: i) otimização baseada no modelo físico do hardware, e ii) uso de aprendizado por reforço.

<p align="center">
<iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/r9fQ4Fc4SH8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>

Para confirmar sua presença na palestra preencha a formulário https://forms.gle/k2EReBzi1sEMDkxC9.

--------------------------------

# 29/10 - 10:00h
#### Computação Quântica - Por que agora?

> [**Ronan Damasco**](#ronan-damasco) - Microsoft

A palestra abordará os recentes avanços na área de tecnologia quântica e de computação quântica fazendo uma reflexão sobre os resultados que podemos esperar nos próximos anos utilizando como referência as experiências anteriores de desenvolvimento e adoção de tecnologias avançadas.

<p align="center">
<iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/Ar0VO7r54RI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>

Para confirmar sua presença na palestra preencha a formulário https://forms.gle/QHnWKKhTTWXFN4MJA.

--------------------------------

# 29/10 - 14:00h
#### Quantum Computing e Azure Quantum - Ferramentas e um guia para desenvolvedores

> [**Waldemir Cambiucci**](#waldemir-cambiucci) - Microsoft

A Microsoft tem anunciado diversas novidades sobre sua iniciativa de Computação Quântica, desde o Ignite 2019. Mais recentemente, tem publicado sobre novidades como parceiros de soluções e startups, parceiros de hardware como IONQ, HONEYWELL, QCI e MICROSOFT, simuladores de quantum, quantum dev kit, katas, workshops e diversas linhas de aprendizado e bibliotecas para casos de uso. Assim, a presente sessão tem por objetivo atualizá-lo sobre as novidades do Azure Quantum, apresentando um roadmap de estudos para você iniciar nesse novo modelo computacional que está nascendo.

<p align="center">
<iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/DnWnaLVsXoY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>

Para confirmar sua presença na palestra preencha a formulário https://forms.gle/mw2S9jv28vHnKN3P9.

--------------------------------
