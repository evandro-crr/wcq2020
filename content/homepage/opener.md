---
title: 'Sobre'
weight: 1
---

Desta vez online, o III Workshop de Computação Quântica reúne nomes da academia
e indústria com o objetivo de atualizar a comunidade brasileira sobre os
rápidos avanços da segunda revolução quântica. Tal revolução baseia-se no uso
da mecânica quântica para construção de novas tecnologias de processamento e
comunicação, que trazem eficiência e segurança sem precedência para a
tecnologia clássica. Embora em estágio embrionário, computadores quânticos já
superaram o poder de supercomputadores e tecnologias quânticas são cada vez
mais relevantes para a economia mundial.  

O Grupo de Computação Quântica da UFSC convida você a participar desta
revolução cientifica e tecnológica! Apresentaremos os recentes avanços na área
através de palestras ao vivo. Abaixo temos mais informações.

### [Inscreva-se!](#inscrição)

Mais informações sobre as plataformas usadas para acompanhar o workshop
serão divulgadas em breve. Não deixe de se inscrever.

### Como participar

Para participar do evento basta se [inscrever](#inscrição) e acompanhar as palestras ao vivo pelo YouTube (links na [programação](#programação)). Durante as apresentações será possível fazer perguntas (inclusive de maneira anônima) e votar em perguntas de outras pessoas utilizando a plataforma Slido. Ao final de cada sessão, as perguntas mais votadas serão feitas ao palestrante. 

Para participar das sessões de perguntas e respostas basta acessar
https://app.sli.do/event/d68c0j97 ou escanear o QR code abaixo apresentado no início de cada palestra.

![slido](fotos/qr.png)

Afora as palestras, também é possível interagir com a organização e outros participantes através do canal do evento no Slack do Brazil Quantum. Acesse https://join.slack.com/t/brazilquantum/shared_invite/zt-htli8j7r-5GiwmkpvzSA8xv33h2QGiQ.