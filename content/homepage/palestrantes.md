---
title: 'Palestrantes'
weight: 4
header_menu: true
---

--------------------------------------

# André Carvalho
> Q-CTRL

![André Carvalho](fotos/andre.jpg)

Andre Carvalho é o Quantum Professional Services Lead na Q-CTRL. Ele completou seu PhD na Universidade Federal do Rio de Janeiro antes de ingressar no Instituto Max-Planck de Física de Sistemas Complexos em Dresden como um pós-doutorado. Ele então passou mais de uma década como pesquisador na Australian National University, onde trabalhou em várias disciplinas, reunindo a experiência de físicos e engenheiros para desenvolver soluções de controle para problemas de tecnologia quântica. Ele também trabalhou como pesquisador sênior na Griffith University. Andre tem uma vasta experiência internacional e é amplamente reconhecido por suas contribuições ao campo do controle quântico.

--------------------------------------

# Leandro Aolita
> Instituto de Física da Universidade Federal do Rio de Janeiro

![Leandro Aolita](fotos/leandro.jpg)

Licenciado em Física pela Universidade de Buenos Aires, Argentina, em 2004 e Doutor em Física pela Universidade Federal do Rio Janeiro, em 2008. Vencedor do edital de um milhão de reais do Instituto Serrapilheira com o projeto "Redes quânticas não confiáveis". Atualmente é professor adjunto do Departamento de Física e Matemática da Universidade Federal do Rio de Janeiro e líder do grupo de computação quântica, que faz parte do maior pólo de ótica quântica e informação quântica da América Latina.  Suas principais linhas de pesquisa são Computação Quântica e Informação Quântica.

--------------------------------------

# Ronan Damasco
> Microsoft 

![Ronan Damasco](fotos/ronan.jpg)

Ronan Damasco é Diretor Nacional de Tecnologia da Microsoft no Brasil. 
Está na Microsoft há 26 anos onde ocupou diversos cargos, entre eles o de Diretor de Educação e Diretor para a América Latina da Engenharia de Suporte. 
É formado em Engenharia Elétrica pela Universidade de Brasília, mestre em Segurança da Informação pela Universidade de Liverpool e possui diversas certificações acadêmicas e profissionais. 
Fez o Programa de Desenvolvimento Gerencial na Fundação Dom Cabral e o Programa de Liderança da Microsoft na Universidade Georgetown. 
Foi também professor de Ciência da Computação da Universidade Católica de Brasília de 1991 a 1998.

--------------------------------

# Waldemir Cambiucci
> Microsoft 

![Waldemir Cambiucci](fotos/waldemir.jpg)

Waldemir Cambiucci é Diretor do Microsoft Technology Center em São Paulo e Tech Sales Manager para o time de especialistas técnicos com foco em Modern Work, Security, Compliance e Business Application na Microsoft Brasil. Veterano de 18 anos de Microsoft e mais de 25 anos de experiência em TI, Waldemir é responsável por discussões sobre Transformação Digital com clientes corporativos da Microsoft, cobrindo tecnologias como Internet das Coisas, Inteligência Artificial, Machine Learning, Serviços Cognitivos, Segurança, LGPD e Quantum Computing.
Waldemir é graduado em Engenharia de Computação pela Escola Politécnica da Universidade de São Paulo (EPUSP), mestre em Engenharia Elétrica pela EPUSP, Pós-Graduado em Finanças Corporativas pelo Instituto Mauá de Tecnologia e certificado em Administração pelo programa FGVPEC in company com a FGV-SP e Microsoft. Também possui as certificações Microsoft-INSEAD Online Business Strategy and Financial Acumen e  Challenging Customers through Business Model Innovation, IOT Certified Professional pela IOT-INC, Microsoft Certified Trainer 2020-2021, Microsoft Certified Educator e Microsoft Certified Azure AI Engineer Associate.

--------------------------------
